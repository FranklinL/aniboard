from django import test
from django.utils import timezone
from django.core.files import uploadedfile
import datetime
import requests
import requests_mock
from .. import models, tasks


class TasksTestCase(test.TestCase):
    def setUp(self):
        # create a couple of test shows an it's sources
        mock_image = uploadedfile.SimpleUploadedFile(
            'mock',
            b'content',
            'image/png'
        )

        # sources
        source1 = models.Source.objects.create(name='Source1')
        source2 = models.Source.objects.create(name='Source2')
        source3 = models.Source.objects.create(name='Source3')

        # shows
        show1 = models.Show.objects.create(
            name='Show1',
            slug='show1',
            cover_image=mock_image,
        )
        show2 = models.Show.objects.create(
            name='Show2',
            slug='show2',
            cover_image=mock_image,
        )

        # showsources
        models.ShowSource.objects.create(
            show=show1,
            source=source1,
        )
        models.ShowSource.objects.create(
            show=show1,
            source=source2,
        )
        models.ShowSource.objects.create(
            show=show1,
            source=source3,
        )
        models.ShowSource.objects.create(
            show=show2,
            source=source1,
        )
        models.ShowSource.objects.create(
            show=show2,
            source=source2,
        )
        models.ShowSource.objects.create(
            show=show2,
            source=source3,
        )

        # backend
        models.Backend.objects.create(
            backend_name='hummingbird',
            name='Hummingbird',
            description='',
            logo=mock_image
        )

    def tearDown(self):
        models.ShowSource.objects.all().delete()
        models.Show.objects.all().delete()
        models.Source.objects.all().delete()

    def test_update_last_aired_episodes(self):
        # two sources air right now, one before and another after
        models.ShowSource.objects.get(
            show__name='Show1',
            source__name='Source1'
        ).airing_datetimes.create(
            airing_datetime=timezone.now(),
        )
        models.ShowSource.objects.get(
            show__name='Show1',
            source__name='Source2'
        ).airing_datetimes.create(
            airing_datetime=timezone.now(),
        )
        models.ShowSource.objects.get(
            show__name='Show2',
            source__name='Source1'
        ).airing_datetimes.create(
            airing_datetime=timezone.now() -
            datetime.timedelta(hours=2),
        )
        models.ShowSource.objects.get(
            show__name='Show2',
            source__name='Source2'
        ).airing_datetimes.create(
            airing_datetime=timezone.now() +
            datetime.timedelta(hours=2)
        )

        # run method
        tasks.update_last_aired_episodes()

        # check that the episode count is right
        self.assertEqual(
            models.ShowSource.objects.filter(
                show__name='Show1',
                last_aired_episode=1
            ).count(), 2
        )
        self.assertEqual(
            models.ShowSource.objects.filter(last_aired_episode=0).count(),
            4
        )

    def test_update_airing_datetimes(self):
        today = timezone.now()
        next_week = today + datetime.timedelta(days=14 - today.weekday())

        # update the stars on date to next week
        models.ShowSource.objects.update(starts_on=today)

        # one haves a weekly schedule
        models.ShowSource.objects.get(
            show__name='Show1',
            source__name='Source1'
        ).schedules.create(tuesday=today.time())

        # one has a monthly schedule
        show_source = models.ShowSource.objects.get(
            show__name='Show1',
            source__name='Source2'
        )
        show_source.schedules.create(saturday=today.time())
        show_source.schedules.create(order=1)
        show_source.schedules.create(order=2)
        show_source.schedules.create(order=3)

        # one has a weekly schedule, but has already finished airing
        show_source = models.ShowSource.objects.get(
            show__name='Show1',
            source__name='Source3'
        )
        show_source.show.number_episodes = 1
        show_source.show.save()
        show_source.last_aired_episode = 1
        show_source.save()
        show_source.schedules.create(saturday=today.time())

        # one has a bi-weekly(twice a week) schedule
        models.ShowSource.objects.get(
            show__name='Show2',
            source__name='Source1'
        ).schedules.create(monday=today.time(), sunday=today.time())

        # one has a bi-monthly(twice a month) schedule
        show_source = models.ShowSource.objects.get(
            show__name='Show2',
            source__name='Source2'
        )

        show_source.schedules.create(wednesday=today.time())
        show_source.schedules.create(order=1)

        # the last one has a bi-weekly schedule but finishes after the first
        # one has a weekly schedule, but has already finished airing
        show_source = models.ShowSource.objects.get(
            show__name='Show2',
            source__name='Source3'
        )
        show_source.show.number_episodes = 3
        show_source.show.save()
        show_source.last_aired_episode = 2
        show_source.save()
        show_source.schedules.create(monday=today.time())
        show_source.schedules.create(order=1, sunday=today.time())

        # run method
        tasks.update_airing_datetimes()

        # check that the airing datetimes were created
        airing_datetimes = models.ShowSource.objects.get(
            show__name='Show1',
            source__name='Source1'
        ).airing_datetimes.all()
        self.assertEqual(
            airing_datetimes[0].airing_datetime.date(),
            (next_week + datetime.timedelta(days=1)).date()
        )

        airing_datetimes = models.ShowSource.objects.get(
            show__name='Show1',
            source__name='Source2'
        ).airing_datetimes.all()
        self.assertEqual(
            airing_datetimes[0].airing_datetime.date(),
            (next_week + datetime.timedelta(days=19)).date()
        )
        airing_datetimes = models.ShowSource.objects.get(
            show__name='Show1',
            source__name='Source3'
        ).airing_datetimes.all()
        self.assertFalse(airing_datetimes)

        airing_datetimes = models.ShowSource.objects.get(
            show__name='Show2',
            source__name='Source1'
        ).airing_datetimes.all()
        self.assertEqual(
            airing_datetimes[0].airing_datetime.date(),
            next_week.date()
        )
        self.assertEqual(
            airing_datetimes[1].airing_datetime.date(),
            (next_week + datetime.timedelta(days=6)).date()
        )

        airing_datetimes = models.ShowSource.objects.get(
            show__name='Show2',
            source__name='Source2'
        ).airing_datetimes.all()
        self.assertEqual(
            airing_datetimes[0].airing_datetime.date(),
            (next_week + datetime.timedelta(days=2)).date()
        )
        airing_datetimes = models.ShowSource.objects.get(
            show__name='Show2',
            source__name='Source3'
        ).airing_datetimes.all()
        self.assertEqual(
            len(airing_datetimes),
            1
        )
        self.assertEqual(
            airing_datetimes[0].airing_datetime.date(),
            next_week.date()
        )

    def test_update_finished_airing_shows(self):
        # set number of episodes
        models.Show.objects.all().update(number_episodes=10)
        models.ShowSource.objects.all().update(last_aired_episode=9)

        # one of the sources of show1 is completed
        models.ShowSource.objects.filter(
            show__name='Show1',
            source__name='Source1'
        ).update(last_aired_episode=10)

        # run method
        tasks.update_finished_airing_shows()

        # check airing state
        self.assertFalse(models.Show.objects.get(name='Show1').airing)
        self.assertTrue(models.Show.objects.get(name='Show2').airing)

    def test_delete_finished_airing_shows(self):
        # set airing false
        models.Show.objects.all().update(airing=False)

        # one of the sources of show 1 aired 21 days ago
        models.ShowSource.objects.get(
            show__name='Show1',
            source__name='Source1'
        ).airing_datetimes.create(
            airing_datetime=timezone.now() - datetime.timedelta(days=21)
        )

        # and one of the sources of show 2 aired 19 days ago
        models.ShowSource.objects.get(
            show__name='Show2',
            source__name='Source1'
        ).airing_datetimes.create(
            airing_datetime=timezone.now().replace(
                hour=0, minute=0, second=0
            ) - datetime.timedelta(days=19)
        )

        tasks.delete_finished_airing_shows()

        # check existence of shows
        self.assertFalse(models.Show.objects.filter(name='Show1').exists())
        self.assertTrue(models.Show.objects.filter(name='Show2').exists())

    @requests_mock.mock()
    def test_update_missing_episode_count_success(self, mock):
        show_id = 100
        episode_count = 20
        show = models.Show.objects.get(slug='show1')

        # add api data for one of the shows
        show.api_data.create(
            show=models.Show.objects.get(slug='show1'),
            backend=models.Backend.objects.get(backend_name='hummingbird'),
            api_id=show_id
        )

        # generate a mockup for the show id
        mock.get(
            'https://hummingbird.me/api/v1/anime/{}'.format(show_id),
            json={
                'episode_count': episode_count
            }
        )

        # run the task
        tasks.update_missing_episode_count()

        # refresh the model and check if the ep count is correct
        show = models.Show.objects.get(pk=show.pk)
        self.assertEqual(
            show.number_episodes, 20, 'The number of episodes is correct'
        )

    @requests_mock.mock()
    def test_update_missing_episode_count_fail(self, mock):
        show_id = 100
        show = models.Show.objects.get(slug='show1')

        # add api data for one of the shows
        show.api_data.create(
            show=models.Show.objects.get(slug='show1'),
            backend=models.Backend.objects.get(backend_name='hummingbird'),
            api_id=show_id
        )

        # generate a mockup for the show id
        mock.get(
            'https://hummingbird.me/api/v1/anime/{}'.format(show_id),
            status_code=requests.codes.server_error
        )

        # run the task
        tasks.update_missing_episode_count()

        # refresh the model and check if the ep count is correct
        show = models.Show.objects.get(pk=show.pk)
        self.assertIsNone(
            show.number_episodes, 'The number of episodes didn\'t update'
        )
