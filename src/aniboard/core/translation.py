from modeltranslation import translator
from . import models


class ShowTranslationOptions(translator.TranslationOptions):
    fields = ('name',)


class SourceTranslationOptions(translator.TranslationOptions):
    fields = ('name',)


translator.translator.register(models.Show, ShowTranslationOptions)
translator.translator.register(models.Source, SourceTranslationOptions)
