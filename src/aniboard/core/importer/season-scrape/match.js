module.exports = function(one, two) {
  one = one.toUpperCase().normalize('NFKC').trim();
  two = two.toUpperCase().normalize('NFKC').trim();
  let pure = size(one, two);
  
  let original = therest(pure);
  if(original > 0.45)
    return original;
  
  let clean = cleanArr(pure);
  clean = therest(size(clean[0], clean[1]));
  if(clean > 0.45)
    return clean;
  
  return original;
}

function cleanArr(inp) {
  let out = JSON.parse(JSON.stringify(inp));
  for(let i in out)
    out[i] = clean(out[i]);
  
  return out;
}

function clean(inp) {
  inp = inp.replace(/[~‐-]/g, ' ');
  //inp = inp.replace(/（.+/i, ' ');
  
  return inp.trim();
}

function size(one, two) {
  var inp = [];
  if(one.length >= two.length) {
    inp.push(one);
    inp.push(two);
  } else {
    inp.push(two)
    inp.push(one)
  }
  
  return inp;
}

function therest(inp) {
  var fuzz = {
    y: 0,
    n: 0
  }

  fuzz.n += inp[0].length - inp[1].length

  var _n = [];
  var match = function(x, y, v) {
    let _y = [];
    while(y < inp[1].length && y >= 0 && x < inp[0].length && x >= 0) {
      if(inp[0][x] != inp[1][y]) {
        _n.push(1 + _n.length);
        if(x + v < inp[0].length && x + v >= 0)
          match(x + v, y, v);
        break;
      }
      if(_n.length > 0)
        for(let i of _n) fuzz.n += i;
      _y.push(1 + _y.length);
      x += v; y += v;
    }
    for(let i of _y) fuzz.y += i;
  }


  match(0, 0, 1);
  if(_n.length > 0) {
    for(let i of _n) fuzz.n += i;
    _n = [];
  }

  var nfuzz = JSON.parse(JSON.stringify(fuzz));
  fuzz.y = 0;
  fuzz.n = 0;
  fuzz.n += inp[0].length - inp[1].length
  nfuzz.fuzz = nfuzz.y / (nfuzz.y + nfuzz.n);

  match(inp[0].length - 1, inp[1].length - 1, -1);
  if(_n.length > 0) {
    for(let i of _n) fuzz.n += i;
    _n = [];
  }


  fuzz.fuzz = fuzz.y / (fuzz.y + fuzz.n);

  var res = ((fuzz.fuzz > nfuzz.fuzz)? fuzz.fuzz : nfuzz.fuzz); //>= 0.45;
  return res;
}
