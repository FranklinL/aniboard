const request = require('request');
const cheerio = require('cheerio');
const keys = require('./keys.js');

const URL = (spec) => `https://www.livechart.me/${spec}`;

module.exports = (spec, cb) => {
  let url = URL(spec? spec : '');
  
  request({url: url}, (err, res, body) => {
    let $ = cheerio.load(body);
    
    let season = $('.chart-title').text().match(/(.+) Anime/i)[1];
    console.log(season);

    getShows($('.anime-card'), (shows) => {
      cb(shows);
    })
  })
}

function getShows(card, cb) {
  let shows = [];
  
  let amt = card.length;
  let count = 0;
  card.each(function(i) {
    let $ = cheerio.load(this);
    let show = {
      title: {},
      id: {},
      eps: null,
      image: null,
      sources: []
    };
    
    let title = $('.main-title');
    show.title.ro = title.attr('data-romaji');
    show.title.ja = title.attr('data-japanese');
    show.title.en = title.attr('data-english');
    
    let eps = $('.anime-episodes').text().match(/(\d+) eps/);
    if(eps) show.eps = parseInt(eps[1]);
    
    let links = [
      '.mal-icon', '.anime-planet-icon', '.anidb-icon', '.hummingbird-icon',
      '.crunchyroll-icon', '.funimation-icon', '.daisuki-icon'
    ];
    for(let link of links) {
      if($(link).length) {
        let url = $(link).attr('href');
        if(/myanimelist/i.test(url))
          show.id.mal = url.match(/\/anime\/(\d+)/i)[1];
        else if(/anime-planet/i.test(url))
          show.id.ap = url.match(/\/anime\/([^\/]+)/i)[1];
        else if(/anidb/i.test(url))
          show.id.adb = url.split('.net/')[1];
        else if(/hummingbird/i.test(url))
          show.id.hb = url.match(/\/anime\/([^\/]+)/i)[1];
        else if(/crunchyroll/i.test(url))
          show.sources.push({
            'name': 'Crunchyroll',
            'id': url.match(/\.com\/([^\/]+)/i)[1],
            'eps': []
          });
        else if(/funimation/i.test(url))
          show.sources.push({
            'name': 'FUNimation',
            'id': url.match(/shows\/([^\/]+)/i)[1],
            'eps': []
          });
        else if(/daisuki/i.test(url))
          show.sources.push({
            'name': 'DAISUKI',
            'id': url.match(/detail\/([^\/]+)/i)[1],
            'eps': []
          });
      }
    }
    
    if($('.preview-icon').length) {
      getImg($('.preview-icon').attr('href'), (img) => {
        show.image = img;
        finish();
      })
    } else {
      finish();
    }
    function finish() {
      shows.push(show);
      count++;
      console.log(`${count}/${amt}: ${show.title.ro}`);
      if(amt == count)
        cb(shows);
    }
  })
}

function getImg(pv, cb) {
  if(/youtu\.?be/i.test(pv)) {
    ytimgURL(pv.match(/(?:v=|\.be\/)([a-zA-Z0-9_-]{11})/)[1], function(img) {
      cb(img);
    });
  } else if(/dailymotion/i.test(pv)) {
    dmimgURL(pv.match(/video\/([a-zA-Z0-9]+)/)[1], function(img) {
      cb(img);
    });
  } else
    cb(null);
}

function dmimgURL(id, cb) {
  request({
    url:`https://api.dailymotion.com/video/${id}`,
    qs: {
      fields: 'thumbnail_url'
    }
  }, function(err, res, body) {
    let data = JSON.parse(body);
    cb(data.thumbnail_url);
  });
}
function ytimgURL(id, cb) {
  request({
    url: 'https://www.googleapis.com/youtube/v3/videos',
    qs: {
      part: 'snippet',
      id: id,
      fields: 'items/snippet/thumbnails',
      key: keys.google
    }
  }, function(err, res, body) {
    let data = JSON.parse(body).items[0];
    if(!data) {
      cb(null);
      return;
    }
    data = data.snippet.thumbnails;
    
    if(data.maxres)
      cb(data.maxres.url);
    else if(data.standard)
      cb(data.standard.url);
    else if(data.high)
      cb(data.high.url);
    else
      cb(null);
  });
}
