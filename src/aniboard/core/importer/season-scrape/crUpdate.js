const request = require('request');
const cheerio = require('cheerio');
const moment = require('moment');

module.exports = (show_sources, cb) => {
  console.log(show_sources);
  console.log('Connecting to Crunchyroll...');
  let update = {
    'updated': [],
    'not_updated': []
  };
  request({
    url: 'http://www.crunchyroll.com/simulcastcalendar',
    qs: {
      filter: 'premium',
      date: moment().format('YYYY-MM-DD')
    }
  }, (err, res, body) => {
    console.log('Retrieved from Crunchyroll!');
    let $ = cheerio.load(body);
    
    let today = $('.today .specific-date > time').text().trim();
    console.log(today);
    
    let releases = [];
    $('.release').each(function() {
      let release = parseRelease($(this));

      if (release) {
        if (releases[release.id] !== undefined) {
          releases[release.id].eps.push(...release.eps);
        } else {
          releases[release.id] = release;
        }
      }
    });
    console.log(releases);
    
    for(let show_source of show_sources) {
      let release = releases[show_source.id];
      if(release !== undefined) {
        let diff = findDiff(show_source, release);

        if(diff) {
          update.updated.push(diff);
        } else {
          update.not_updated.push(show_source);
        }
      }
    }
    
    cb(update);
  });
};

function parseRelease(item) {
  let release = {
    id: '',
    eps: []
  };
  let title = item.find('cite[itemprop="name"]').first().text();
  if(/dub\)/i.test(title))
    return false;
  
  release['id'] = item.attr('data-slug');
  
  let ep = {
    ep: NaN,
    datetime: null
  }

  // the ep num can contain a dash
  let epNum = item.attr('data-episode-num');
  let dashIndex = epNum.indexOf('-');

  if (dashIndex !== -1) {
    epNum = epNum.substr(dashIndex + 1);
  }

  ep['ep'] = parseInt(epNum, 10);
  let datetime =  item.find('.available-time').first().attr('datetime');
  ep['datetime'] = moment(datetime).unix();
  release['eps'].push(ep);
  
  return release;
}

function findDiff(original, nu) {
  let old = JSON.parse(JSON.stringify(original));
  
  thrunu:
  for(let nep of nu['eps']) {
    for(let oep in old['eps']) {
      if(old['eps'][oep]['ep'] === nep['ep']) {
        old['eps'][oep]['datetime'] = nep['datetime'];
        continue thrunu;
      }
    }
    old['eps'].push(nep);
  }
  
  if(JSON.stringify(old) === JSON.stringify(original))
    return false;
  else
    return old;
}
