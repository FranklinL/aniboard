from django.utils import timezone
from django.core.files import base as files
import abc
from aniboard.core import models


class ParseException(Exception):
    pass


class PreprocessException(Exception):
    pass


class PostprocessException(Exception):
    pass


class SaveException(Exception):
    pass


class BaseParser(metaclass=abc.ABCMeta):
    # error template
    error_template = 'Entry number {}: {}'

    def build_error(self, number, text):
        return self.error_template.format(number, text)

    def parse(self, import_file):
        """ Entry point for the parsing and creating process. """
        import_text = import_file.read()

        if type(import_text) is bytes:
            import_text = import_text.decode('utf-8')

        errors = []
        added = []
        updated = []

        # parse entries and get errors
        entries = self.parse_entries(import_text)
        fails = self.parse_fails(import_text)

        for number, entry in enumerate(entries):
            if 'error' in entry:
                errors.append(entry['error'])
                continue

            try:
                self.preprocess_entry(entry)
            except PreprocessException as ex:
                errors.append(self.build_error(number, str(ex)))
                continue

            # check if the show doesn't exist already
            show = models.Show.objects.filter(
                syoboi_id=entry['id']['sb']
            ).first()

            try:
                if show:
                    # update already existing show
                    self.update_show(entry, show)
                    updated.append(show)
                else:
                    # create show and associated models
                    show = self.create_show(entry)
                    added.append(show)
            except SaveException as ex:
                errors.append(self.build_error(number, str(ex)))

            # postprocess the entry
            self.postprocess_entry(entry)

        return added, updated, errors, fails

    def update_image(self, img_link, img_content, show):
        """ Updates the image for a show. """
        # the image needs some extra processing
        if not show.cover_image:
            show.cover_image.save(
              models.cover_image_path(show, img_link.split('/')[-1]),
              files.ContentFile(img_content)
            )

    def update_api_datas(self, backends, show):
        """ Creates and/or updates the related api data. """
        for backend_name, api_id in backends.items():
            backend = models.Backend.objects.filter(
                backend_name=backend_name
            ).first()

            if backend is not None:
                api_data, _ = show.api_data.get_or_create(
                    backend=backend
                )
                api_data.api_id = api_id
                api_data.save()

    def update_show_source(self, source, show):
        """ Updates the show source for a show. """
        show_source = None

        if source['source']:
            show_source, created = show.show_sources.get_or_create(
                show=show, source=source['source']
            )

        # premiere
        show_source.is_premiere = source['is_premiere']

        if 'eps' in source and len(source['eps']) > 0:
            episode_number = None

            # search for the first future ep
            last_ep = next(
                (ep for ep in source['eps']
                 if ep['datetime'] > timezone.now()),
                None
            )

            if last_ep is not None:
                if last_ep['ep'] is not None:
                    # use the ep number - 1 to get the last aired episode
                    episode_number = last_ep['ep'] - 1
            else:
                # if we found no future ep, use the last ep in the past
                last_ep = source['eps'][-1]
                episode_number = last_ep['ep']

            # if episode_number is not None:
                # show_source.last_aired_episode = episode_number

            airing_datetime = last_ep['datetime']

            if last_ep['ep'] is 1 or show_source.starts_on is None:
                # starts on
                show_source.starts_on = airing_datetime

            show_source.save()

            # create a new schedule and compare it with the old one to
            # check if it needs to change
            schedule = models.AiringSchedule()
            schedule[airing_datetime.weekday()] = airing_datetime.time()

            old_schedule = show_source.schedules.first()

            if old_schedule is None or \
               list(schedule) != list(old_schedule):
                # delete the old schedule and add the new one
                if old_schedule is not None:
                    old_schedule.delete()

                show_source.schedules.add(schedule)

                # delete old unused airing datetimes and create a new ones
                show_source.airing_datetimes.filter(used=False).delete()
                show_source.create_airing_datetimes(weeks=2)

    @abc.abstractmethod
    def parse_entries(self, import_text):
        raise NotImplementedError()

    @abc.abstractmethod
    def preprocess_entry(self, entry):
        raise NotImplementedError()

    @abc.abstractmethod
    def create_show(self, entry):
        """ Creates the show and associated models from the parsed and
        processed entry """
        raise NotImplementedError()

    @abc.abstractmethod
    def update_show(self, entry, show):
        raise NotImplementedError()

    @abc.abstractmethod
    def postprocess_entry(self, entry):
        """ Process the entry after it was used to free resources """
        raise NotImplementedError()
