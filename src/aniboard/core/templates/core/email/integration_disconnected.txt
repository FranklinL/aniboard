{% extends 'core/email/base.txt' %}
{% block content %}
Your integration has been disconnected given an authentication error occurred when trying to use the site's API. This could mean that you changed your password on the site or simply that the authentication token has expired.

Try connecting your account once again or feel free to contact our admins through the feedback button on the site if you want to get more information about the issue or if the issue persist even after reconnecting your account.
{% endblock %}
