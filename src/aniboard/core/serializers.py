from django.contrib.auth import models as auth_models
from django.utils import timezone
from rest_framework import serializers
import datetime
import types

from . import models


class RestoreFromDBMixin(object):
    def to_internal_value(self, data):
        return self.Meta.model.objects.get(**{'pk': data})


def restore_from_db_factory(klass, **options):
    name = '{}AuxRestoreFromMixin'.format(klass.__name__)

    def populate_options(aux_class):
        class Meta(klass.Meta):
            pass

        for option, value in options.items():
            setattr(Meta, option, value)

        aux_class['Meta'] = Meta

    aux_class = types.new_class(
        name, (RestoreFromDBMixin, klass), exec_body=populate_options
    )

    return aux_class


class UserSerializer(serializers.ModelSerializer):
    is_victim = serializers.SerializerMethodField('_is_victim')

    class Meta:
        model = auth_models.User
        fields = ('id', 'username', 'is_victim')

    def _is_victim(self, user):
        stalker = self.context['request'].user

        return stalker.is_authenticated() and user.stalked_by.filter(
            stalker=stalker,
        ).exists()

EmbeddedUserSerializer = restore_from_db_factory(UserSerializer)


class ShowSerializer(serializers.ModelSerializer):
    cover_image_url = serializers.ReadOnlyField()

    class Meta:
        model = models.Show
        fields = (
            'id', 'name', 'slug', 'airing', 'cover_image_url',
            'number_episodes', 'show_sources', 'score', 'score_count',
            'added_date'
        )

EmbeddedShowSerializer = restore_from_db_factory(ShowSerializer)


class ShowEpisodeSerializer(serializers.ModelSerializer):
    has_comment = serializers.SerializerMethodField('_has_comment')

    class Meta:
        model = models.ShowEpisode
        fields = ('id', 'show', 'episode', 'score', 'has_comment')

    def _has_comment(self, episode):
        user = self.context['request'].user
        has_comment = False

        if user.is_authenticated():
            has_comment = models.UserShowComment.objects.filter(
                user_show__user=user,
                user_show__show_source__show=episode.show,
                episode=episode.episode
            ).exists()

        return has_comment


class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Source
        fields = ('id', 'name')

EmbeddedSourceSerializer = restore_from_db_factory(SourceSerializer)


class AiringDatetimesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AiringDatetime
        fields = ('id', 'airing_datetime')

EmbeddedAiringDatetimesSerializer = restore_from_db_factory(
    AiringDatetimesSerializer
)


class ListAiringDatetimesSerializer(serializers.ListSerializer):
    child = EmbeddedAiringDatetimesSerializer()

    def get_attribute(self, obj):
        # we filter the datetimes from yesterday on, to avoid retrieving all
        # of them, but taking into account that it may be still yesterday
        # in some places
        yesterday = timezone.now().replace(
            hour=0, minute=0, second=0, microsecond=0,
        ) - datetime.timedelta(days=1)

        airing_datetimes = obj.airing_datetimes.filter(
            skip=False,
            airing_datetime__gte=yesterday,
        )

        return airing_datetimes


class ShowSourceSerializer(serializers.ModelSerializer):
    airing_datetimes = ListAiringDatetimesSerializer()
    source = EmbeddedSourceSerializer()

    class Meta:
        model = models.ShowSource
        fields = (
            'id', 'source', 'last_aired_episode', 'airing_datetimes',
            'is_premiere'
        )


class EmbeddedShowSourceSerializer(RestoreFromDBMixin, ShowSourceSerializer):
    show = EmbeddedShowSerializer()

    class Meta:
        model = models.ShowSource
        fields = (
            'id', 'show', 'source', 'airing_datetimes', 'last_aired_episode'
        )


class UserShowSerializer(serializers.ModelSerializer):
    show_source = EmbeddedShowSourceSerializer()

    class Meta:
        model = models.UserShow
        fields = (
            'id', 'show_source', 'last_episode_watched', 'dropped', 'rating',
            'score'
        )

EmbeddedUserShowSerializer = restore_from_db_factory(UserShowSerializer)


class UserShowCommentSerializer(serializers.ModelSerializer):
    user = EmbeddedUserSerializer(source='user_show.user', read_only=True)

    class Meta:
        model = models.UserShowComment
        fields = (
            'id', 'user_show', 'user', 'episode', 'modified', 'liked',
            'comment'
        )

    def validate(self, data):
        if data['episode'] > data['user_show'].last_episode_watched:
            raise serializers.ValidationError(
                'You can\'t comment on episodes you haven\'t watched yet'
            )

        return data

EmbeddedUserShowCommentSerializer = restore_from_db_factory(
    UserShowCommentSerializer
)


class UserShowActionSerializer(serializers.ModelSerializer):
    user_show = EmbeddedUserShowSerializer()
    comment = EmbeddedUserShowCommentSerializer()

    class Meta:
        model = models.UserShowAction
        fields = (
            'id', 'user_show', 'episode', 'action_datetime', 'comment'
        )


class BackendSerializer(serializers.ModelSerializer):
    is_connected = serializers.SerializerMethodField('_is_connected')

    class Meta:
        model = models.Backend
        fields = (
            'id', 'backend_name', 'name', 'description', 'is_connected',
            'logo_url', 'auth_url'
        )

    def _is_connected(self, backend):
        user = self.context['request'].user
        is_connected = False

        if user.is_authenticated():
            is_connected = user.api_data.filter(backend=backend).exists()

        return is_connected


class SettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Settings
        fields = ('id', 'ask_rating', 'ask_comment', 'theme')


class UserTraceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserTrace
        fields = ('trace',)


class FeedbackSerializer(serializers.Serializer):
    type = serializers.CharField()
    short_description = serializers.CharField()
    details = serializers.CharField()


class UserRegistrationSerializer(serializers.ModelSerializer):
    username = serializers.RegexField(
        r'^[\w.@+-]+$',
        required=True,
        error_messages={
            'invalid': 'a valid username can only contain letters and the '
            'following symbols: .@+-'
        }
    )
    email = serializers.EmailField(required=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = auth_models.User
        fields = ('email', 'username', 'password',)

    def validate_email(self, value):
        if auth_models.User.objects.filter(email=value).exists():
            raise serializers.ValidationError('this email is already in use')

        return value

    def validate_username(self, value):
        if auth_models.User.objects.filter(username=value).exists():
            raise serializers.ValidationError(
                'this username is already in use'
            )

        return value

    def create(self, validated_data):
        user = auth_models.User.objects.create_user(
            validated_data['username'],
            validated_data['email'],
            validated_data['password'],
        )
        user.is_active = False
        user.save()

        return user


class UserActivationSerializer(serializers.Serializer):
    activation_code = serializers.CharField()


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()


class PasswordRecoverySerializer(serializers.Serializer):
    recovery_code = serializers.CharField()
    password = serializers.CharField()


class NoticeSerializer(serializers.ModelSerializer):
    is_read = serializers.SerializerMethodField('_is_read')

    class Meta:
        model = models.Notice
        fields = ('id', 'date_time', 'text', 'is_read')

    def _is_read(self, notice):
        last_read = self.context['request'].user.settings.last_notice_read
        return last_read is not None and notice.id <= last_read.id
