from django import http
from django.test import testcases
from django.views.decorators import http as http_decorators, \
    csrf as csrf_decorators

dummy_testcase = testcases.TransactionTestCase()


@http_decorators.require_POST
@csrf_decorators.csrf_exempt
def test_setup(request):
    fixture = request.POST['fixture']
    dummy_testcase.fixtures = [fixture]
    dummy_testcase._pre_setup()
    return http.HttpResponse('OK')


@http_decorators.require_POST
@csrf_decorators.csrf_exempt
def test_teardown(request):
    dummy_testcase._post_teardown()
    return http.HttpResponse('OK')
