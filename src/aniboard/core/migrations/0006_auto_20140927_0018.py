# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20140927_0004'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='usershow',
            options={},
        ),
        migrations.RemoveField(
            model_name='showsource',
            name='next_airing_datetime',
        ),
        migrations.RemoveField(
            model_name='showsource',
            name='previous_airing_datetime',
        ),
    ]
