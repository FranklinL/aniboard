# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def api_data_to_api_id(apps, schema_editor):
    ShowAPIData = apps.get_model('core', 'ShowAPIData')

    for api_data in ShowAPIData.objects.all():
        api_data.api_id = api_data.data['id']
        api_data.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0026_auto_20150714_0321'),
    ]

    operations = [
        migrations.RunPython(api_data_to_api_id),
        migrations.RemoveField(
            model_name='showapidata',
            name='data',
        ),
        migrations.AlterField(
            model_name='showapidata',
            name='api_id',
            field=models.CharField(max_length=250),
        ),
    ]
