# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_auto_20150126_0003'),
    ]

    operations = [
        migrations.AddField(
            model_name='settings',
            name='theme',
            field=models.CharField(blank=True, default='default', max_length=100, null=True),
            preserve_default=True,
        ),
    ]
