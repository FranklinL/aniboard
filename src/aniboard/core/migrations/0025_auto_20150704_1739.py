# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import aniboard.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_usershowaction_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='show',
            name='cover_image',
            field=models.ImageField(upload_to=aniboard.core.models.cover_image_path, blank=True, max_length=200, null=True),
            preserve_default=True,
        ),
    ]
