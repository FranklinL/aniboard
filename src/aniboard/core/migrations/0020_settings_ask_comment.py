# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0019_showepisode'),
    ]

    operations = [
        migrations.AddField(
            model_name='settings',
            name='ask_comment',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
