# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20150107_1427'),
    ]

    operations = [
        migrations.AddField(
            model_name='airingdatetime',
            name='skip',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
