# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0029_auto_20150914_0231'),
    ]

    operations = [
        migrations.AddField(
            model_name='show',
            name='autoupdate_number_episodes',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
