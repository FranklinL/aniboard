`import Ember from 'ember'`

testServerTeardown = (app) ->
    new Ember.RSVP.Promise (resolve, reject) ->
      Ember.$.post('testserver/teardown/').always -> resolve()

`export default Ember.Test.registerAsyncHelper('testServerTeardown', testServerTeardown)`
