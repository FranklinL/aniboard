import Ember from 'ember';
import EventHub from 'aniboard/structures/event-hub';

function withEventHub(options, klass, container) {
  return klass.create(Ember.merge({eventHub: EventHub.create()}, options));
}

export default withEventHub;
