`import Ember from 'ember'`
`import { module, test } from 'qunit'`
`import startApp from '../helpers/start-app'`

application = null

module 'Acceptance: Stalk section',
  beforeEach: ->
    application = startApp()
    testServerSetup()
    login identification: 'test', password: 'test'
    ###
    Don't return as Ember.Application.then is deprecated.
    Newer version of QUnit uses the return value's .then
    function to wait for promises if it exists.
    ###
    return

  afterEach: ->
    invalidateSession().then ->
      testServerTeardown()
      Ember.run application, 'destroy'

test 'page loads', (assert) ->
  visit '/stalk'

  andThen ->
    assert.equal currentPath(), 'actions.index',
      'We are on the stalk index page'
    assert.equal find('.stalk').length, 1,
      'The stalk section is shown'

test 'add stalking', (assert) ->
  visit '/stalk'
  fillIn '.stalk_search input.search', 'admin'

  andThen ->
    assert.equal find('.search_result').length, 1, 'We find an user'
    assert.equal find('.search_result .stalk').text(), 'stalk',
      'We can stalk him'

  click '.search_result .stalk'

  andThen ->
    assert.equal find('.stalking a').length, 1, 'The user was added'
    assert.equal find('.stalking a').text(), 'admin', 'It\'s the correct user'
    assert.equal find('.search_result .stalk').text(), 'KILL',
      'Now we can kill it'

  # clear up the search
  fillIn '.stalk_search input.search', ''

test 'remove stalking', (assert) ->
  visit '/stalk'

  # stalk a user
  fillIn '.stalk_search input.search', 'admin'
  click '.search_result .stalk'

  # clear up the search
  fillIn '.stalk_search input.search', ''

  andThen ->
    assert.equal find('.stalking a').length, 1, 'We have a user stalked'
    assert.equal find('.stalking a').text(), 'admin', 'It\'s the correct user'

  fillIn '.stalk_search input.search', 'admin'

  andThen ->
    assert.equal find('.search_result .name').text(), 'admin',
      'We find the user'
    assert.equal find('.search_result .stalk').text(), 'KILL',
      'We can kill the user'

  click '.search_result .stalk'

  andThen ->
    assert.equal find('.stalking a').length, 0, 'The user was removed'
    assert.equal find('.search_result .stalk').text(), 'stalk',
      'We can add it again'
