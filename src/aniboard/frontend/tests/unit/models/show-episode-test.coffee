`import { test, moduleForModel } from 'ember-qunit'`

moduleForModel 'show-episode', 'ShowEpisode', {
  # Specify the other units that are required for this test.
  needs: [
    'model:show'
    'model:show-source'
  ]
}

test 'it exists', ->
  model = @subject()
  # store = @store()
  ok !!model
