`import { test, moduleForModel } from 'ember-qunit'`

moduleForModel 'user-show-action', 'UserShowAction', {
  # Specify the other units that are required for this test.
  needs: [
    'model:user'
    'model:user-show'
    'model:show-source'
    'model:show'
    'model:source'
    'model:user-show-comment'
  ]
}

test 'it exists', ->
  model = @subject()
  # store = @store()
  ok !!model
