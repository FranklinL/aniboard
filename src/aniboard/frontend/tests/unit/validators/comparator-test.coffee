`import Ember from 'ember'`
`import { test, moduleFor } from 'ember-qunit'`

model = null

moduleFor 'validator:comparator', 'ComparatorValidator',
  setup: ->
    Model = Ember.Object.extend
      dependentValidationKeys: {}

    Ember.run ->
      model = Model.create()

# Replace this with your real tests.
test 'it exists', ->
  validator = @subject
    model: model
    property: 'attribute'
    options:
      against: 'otherAttribute'
      comparator: -> true
  ok validator
