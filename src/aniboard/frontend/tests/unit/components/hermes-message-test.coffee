`import { test, moduleForComponent } from 'ember-qunit'`

moduleForComponent 'hermes-message', 'HermesMessageComponent', {
  # specify the other units that are required for this test
  # needs: ['component:foo', 'helper:bar']
  needs: ['helper:compile-markdown']
}

test 'it renders', ->
  expect 0
  # expect 2

  # # creates the component instance
  # component = @subject()
  # equal component._state, 'preRender'

  # # appends the component to the page
  # @append()
  # equal component._state, 'inDOM'
