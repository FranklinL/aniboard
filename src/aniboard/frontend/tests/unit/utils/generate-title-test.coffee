`import generateTitle from 'aniboard/utils/generate-title'`

module 'generateTitle'

# Replace this with your real tests.
test 'it works', ->
  result = generateTitle ['test']
  equal result, 'test - Tsuiseki'
