`import { test, moduleFor } from 'ember-qunit'`

moduleFor 'controller:account/login', 'LoginController', {
  # Specify the other units that are required for this test.
  needs: [
    'service:validations'
    'ember-validations@validator:local/presence'
  ]
}

# Replace this with your real tests.
test 'it exists', ->
  controller = @subject()
  ok controller

