`import Ember from 'ember'`

ShowDayListViewMixin = Ember.Mixin.create
  layoutName: 'show/day-list-layout'
  tagName: 'section'
  classNames: ['day_list']
  classNameBindings: ['hide']
  hide: Ember.computed.empty 'controller.model.shows'

`export default ShowDayListViewMixin`
