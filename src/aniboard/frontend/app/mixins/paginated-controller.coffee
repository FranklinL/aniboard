`import Ember from 'ember'`

PaginatedControllerMixin = Ember.Mixin.create
  page: Ember.computed.alias 'model.currentPage'
  pages: Ember.computed.alias 'model.totalPages'
  showPrevious: Ember.computed.gt 'page', 1

  showNext: Ember.computed 'page', 'pages', ->
    @get('page') < @get('pages')

  actions:
    nextPage: ->
      @incrementProperty 'page'

    previousPage: ->
      @decrementProperty 'page'

`export default PaginatedControllerMixin`
