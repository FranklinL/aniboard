`import Ember from 'ember'`

InfinitePaginatedControllerMixin = Ember.Mixin.create
  page: Ember.computed.alias 'model.loadedPage'
  pages: Ember.computed.alias 'model.totalPages'

  showLoad: Ember.computed 'page', 'pages', ->
    @get('page') < @get('pages')

  actions:
    loadNext: ->
      @incrementProperty 'page'

`export default InfinitePaginatedControllerMixin`
