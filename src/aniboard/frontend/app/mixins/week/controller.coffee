`import Ember from 'ember'`

WeekControllerMixin = Ember.Mixin.create
  empty: Ember.computed.empty '[]'
  showsSortAscending: true

  unsortedDays: Ember.computed 'model', ->
    unsorted = []

    for day in [0..8]
      unsorted.push
        day: day
        shows: Ember.ArrayProxy.createWithMixins Ember.SortableMixin,
          content: []
          sortProperties: @get 'showsSortProperties'
          sortFunction: @get('showsSortFunction') day
          sortAscending: @get 'showsSortAscending'

    unsorted

  populateDays: Ember.observer 'model', ->
    @forEach @assignShow, @

  assignShow: (show) ->
    unsortedDays = @get 'unsortedDays'
    daysForShow = @daysForShow show

    for day in daysForShow
      unsortedDays[day].shows.pushObject show

  deassignShow: (show) ->
    unsortedDays = @get 'unsortedDays'
    daysForShow = @daysForShow show

    for day in daysForShow
      unsortedDays[day].shows.removeObject show

`export default WeekControllerMixin`
