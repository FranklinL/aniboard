`import EventHub from '../structures/event-hub'`

# Takes two parameters: container and app
initialize = (container, app) ->
  container.register 'eventHub:main', EventHub
  app.inject 'controller', 'eventHub', 'eventHub:main'
  app.inject 'route', 'eventHub', 'eventHub:main'
  app.inject 'view', 'eventHub', 'eventHub:main'

EventHubInitializer =
  name: 'event-hub'
  initialize: initialize

`export {initialize}`
`export default EventHubInitializer`
