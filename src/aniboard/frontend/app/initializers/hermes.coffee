`import Hermes from '../structures/hermes'`

# Takes two parameters: container and app
initialize = (container, app) ->
  container.register 'hermes:main', Hermes
  app.inject 'controller', 'hermes', 'hermes:main'
  app.inject 'component', 'hermes', 'hermes:main'
  app.inject 'route', 'hermes', 'hermes:main'

HermesInitializer =
  name: 'hermes'
  initialize: initialize

`export {initialize}`
`export default HermesInitializer`
