# Takes two parameters: container and app
initialize = () ->
  # wrappers
  Ember.EasyForm.Config.registerWrapper 'default',
    inputClass: 'stylish-input'

  Ember.EasyForm.Config.registerWrapper 'unstylish',
    inputClass: ''

EasyformStylesInitializer =
  name: 'easyform-wrappers'
  initialize: initialize

`export {initialize}`
`export default EasyformStylesInitializer`
