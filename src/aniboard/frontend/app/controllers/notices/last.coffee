`import Ember from 'ember'`
`import DS from 'ember-data'`

NoticesLastController = Ember.ArrayController.extend
  last: Ember.computed.alias 'lastObject'

  model: Ember.computed 'session.isAuthenticated', ->
    if @get 'session.isAuthenticated'
      DS.PromiseArray.create
        promise: @store.find 'notice',
          latest: true
          page_size: 1
    else
      []

  actions:
    read: ->
      @get('last').read()

`export default NoticesLastController`
