`import Ember from 'ember'`
`import WeekControllerMixin from '../mixins/week/controller'`
`import momentCompare from '../utils/moment-compare'`

UserShowsController = Ember.ArrayController.extend WeekControllerMixin,
  canEdit: true
  showsSortProperties: ['showSource']

  init: ->
    @_super()

    @eventHub.on 'userShow:dropped', @, @removeShow
    @eventHub.on 'userShow:willUpdate', @, @showWillUpdate
    @eventHub.on 'userShow:didUpdate', @, @showDidUpdate

  showsSortFunction: (day) -> (show1, show2) ->
    if day isnt 8
      datetime1 = show1.airingDatetime day
      datetime2 = show2.airingDatetime day

      if datetime1? and datetime2?
        result = momentCompare datetime1, datetime2
      else if datetime1?
        result = -1
      else if datetime2?
        result = 1

    if not result?
      result = Ember.compare show1.get('show.name'), show2.get('show.name')

    result

  daysForShow: (show) ->
    show.get('airingDays')

  days: Ember.computed 'unsortedDays', ->
    unsortedDays = @get 'unsortedDays'

    today = (new Date()).getDay()
    sorted = unsortedDays[today..6]
    sorted = sorted.concat(unsortedDays[0..today - 1]) if today > 0
    sorted = sorted.concat unsortedDays[7]
    sorted = sorted.concat unsortedDays[8]
    sorted

  removeShow: (show) ->
    @removeObject show
    @deassignShow show

  addShow: (show) ->
    @addObject show
    @assignShow show

  showWillUpdate: (show, changes) ->
    if show.get('showSource') != changes.get('showSource')
      # remove the object to avoid quirks
      @removeShow show

  showDidUpdate: (show) ->
    if not @contains show
      # re-add it once it's done animating out
      Ember.run.later @, @addShow, show, 500

  actions:
    watchOne: (show) ->
      show.incrementProperty 'lastEpisodeWatched'
      name = show.get 'name'

      @set 'hermes.info', "Updating **#{name}** watch count..."
      show.save().then( =>
        @set 'hermes.success', "Successfully updated **#{name}** watch count."

        # sent ga event
        @get('ga').send 'event', 'user-show', 'watch', name

        # ask comment/rating
        askComment = @get 'settings.askComment'
        askRating = @get 'settings.askRating'
        askShowRating = show.get 'askRating'

        if askComment
          # ask for comment
          @transitionToRoute 'comments.edit', show,
            show.get('lastEpisodeWatched')
        else if askRating and askShowRating
          # ask for rating
          @transitionToRoute 'userShow.rate', show
      ).catch =>
        @set 'hermes.error', "Failed to update **#{name}** watch count."
        show.rollback()

`export default UserShowsController`
