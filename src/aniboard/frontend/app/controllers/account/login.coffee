`import Ember from 'ember'`
`import EmberValidations from 'ember-validations'`
`import AuthenticationControllerMixin from 'simple-auth/mixins/authentication-controller-mixin'`

LoginController = Ember.Controller.extend AuthenticationControllerMixin,
EmberValidations.Mixin,
  authenticator: 'authenticator:aniboard'
  validations:
    identification:
      presence: true
    password:
      presence: true

  actions:
    authenticate: ->
      data = @getProperties 'identification', 'password'
      @set 'hermes.info', 'Authenticating...'
      @_super(data).catch(->)

`export default LoginController`
