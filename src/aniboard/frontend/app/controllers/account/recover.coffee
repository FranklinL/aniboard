`import Ember from 'ember'`

AccountRecoverController = Ember.Controller.extend
  actions:
    recover: ->
      if not @get 'model.isSaving'
        model = @get 'model'

        # try to save
        @set 'hermes.info','Changing password...'

        model.save().then( =>
          @set 'hermes.success', 'Succesfully changed!'

          # close the dialog
          @send 'closeDialog'
        ).catch =>
          @set 'hermes.error', 'Failed to change'

`export default AccountRecoverController`
