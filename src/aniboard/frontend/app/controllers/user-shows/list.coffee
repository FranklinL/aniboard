`import Ember from 'ember'`
`import ShowDayListControllerMixin from '../../mixins/show/day-list-controller'`

UserShowsListController = Ember.Controller.extend ShowDayListControllerMixin

`export default UserShowsListController`
