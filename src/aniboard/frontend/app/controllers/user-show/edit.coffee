`import Ember from 'ember'`

UserShowEditController = Ember.Controller.extend
  sources: Ember.computed.alias 'model.showSource.show.showSources'

  editable: Ember.computed ->
    @store.createRecord 'userShow'

  actions:
    save: ->
      if not @get 'model.isSaving'
        model = @get 'model'
        editable = @get 'editable'
        name = @get 'model.name'

        # trigger event to indicate that the show will update
        @eventHub.trigger 'userShow:willUpdate', model, editable

        # update content
        oldShowSource = @get 'model.showSource'
        oldLastEp = @get 'model.lastEpisodeWatched'
        lastEp = editable.get 'lastEpisodeWatched'
        showSource = editable.get 'showSource'

        if oldShowSource != showSource or oldLastEp != parseInt(lastEp)
          model.setProperties
            lastEpisodeWatched: lastEp
            showSource: showSource

          # try to save
          @set 'hermes.info', "Updating **#{name}**..."

          model.save().then(=>
            @set 'hermes.success', "Successfully updated **#{name}**."

            # close the dialog
            @send 'closeDialog'
          ).catch(=>
            @set 'hermes.error', "Failed to update **#{name}**."
            model.rollback()
            @set 'model.showSource', oldShowSource
          ).finally =>
            # trigger event to indicate that the show did update
            @eventHub.trigger 'userShow:didUpdate', model

`export default UserShowEditController`
