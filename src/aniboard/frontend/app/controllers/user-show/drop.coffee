`import Ember from 'ember'`

UserShowDropController = Ember.Controller.extend
  actions:
    drop: ->
      name = @get 'model.name'
      model = @get 'model'

      @set 'model.dropped', true

      # try to save
      @set 'hermes.info', "Dropping **#{name}** from your list..."

      model.save().then( =>
        @set 'hermes.success', "Successfully dropped **#{name}**
          from your list."

        # send event indicating we removed the user show
        @eventHub.trigger 'userShow:dropped', model

        # close the dialog
        @send 'closeDialog'
      ).catch =>
        model.rollback()
        @set 'hermes.error', "Failed to drop **#{name}** from your list."

`export default UserShowDropController`
