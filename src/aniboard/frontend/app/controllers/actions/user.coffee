`import Ember from 'ember'`
`import WeekControllerMixin from '../../mixins/week/controller'`
`import momentCompare from '../../utils/moment-compare'`

ActionsUserController = Ember.ArrayController.extend WeekControllerMixin,
  showsSortProperties: ['actionMoment']
  showsSortAscending: false

  showsSortFunction: (day) -> momentCompare

  daysForShow: (show) ->
    [show.get('actionMoment').day()]

  days: Ember.computed 'unsortedDays', ->
    unsortedDays = @get 'unsortedDays'
    today = (new Date()).getDay()
    sorted = unsortedDays[0..today].reverse()
    sorted = sorted.concat(unsortedDays[today + 1..6].reverse()) if today < 6
    sorted

`export default ActionsUserController`
