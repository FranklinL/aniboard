`import Ember from 'ember'`
`import PaginatedControllerMixin from '../../mixins/paginated-controller'`
`import {PaginatedArray} from '../../structures/paginated-array'`
`import DebouncedPropertiesMixin from 'ember-debounced-properties/mixin'`

ActionsSearchController = Ember.ArrayController.extend PaginatedControllerMixin,
DebouncedPropertiesMixin,
  searchTerm: Ember.computed.alias 'model.opts.search'
  previousSearchTerm: ''
  debouncedProperties: ['searchTerm']

  model: Ember.computed ->
    PaginatedArray.create
      store: @store
      modelName: 'user'
      perPage: 2
      fetchOnInit: false
      clearOnFetch: true

  fetch: Ember.observer 'debouncedSearchTerm', ->
    current = @get 'searchTerm'
    previous = @get 'previousSearchTerm'

    if current and current != previous
      @set 'page', 1
      @get('model').fetch()
    else
      @clear()

    @set 'previousSearchTerm', current

  isSearching: Ember.computed 'debouncedSearchTerm', 'model.isPending', ->
    @get('searchTerm') and @get('model.isPending')

  nothingFound: Ember.computed 'debouncedSearchTerm', 'length', 'model.isSettled', ->
    not @get('length') and @get('searchTerm') and @get('model.isSettled')

`export default ActionsSearchController`
