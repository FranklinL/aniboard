`import Ember from 'ember'`
`import InfinitePaginatedController from '../../mixins/infinite-paginated-controller'`

CommentsIndexController = Ember.ArrayController.extend InfinitePaginatedController,
  queryParams:
    filter:
      scope: 'controller'
  filter: null
  episode: Ember.computed.readOnly 'showEpisode.episode'
  showPreviousEpisode: Ember.computed.gt 'episode', 1

  showNextEpisode: Ember.computed 'episode', 'userShow.lastEpisodeWatched', ->
    episode = @get 'episode'
    lastEpisodeWatched = @get 'userShow.lastEpisodeWatched'
    episode < lastEpisodeWatched

  isSelfFilter: Ember.computed.equal 'filter', 'self'

  episodeScore: Ember.computed 'showEpisode.score', 'isSelfFilter', ->
    if not @get 'isSelfFilter'
      @get 'showEpisode.score'

  showScore: Ember.computed 'userShow.score', 'userShow.show.score', 'isSelfFilter', ->
    if @get 'isSelfFilter'
      @get 'userShow.score'
    else
      @get 'userShow.show.score'

  hasEpisodeScore: Ember.computed.notEmpty 'episodeScore'
  hasShowScore: Ember.computed.notEmpty 'showScore'
  hasScore: Ember.computed.or 'hasEpisodeScore', 'hasShowScore'

  episodeMedium: Ember.computed 'episodeScore', ->
    score = @get 'episodeScore'
    score? and score >= 50 and score < 85

  episodePositive: Ember.computed 'episodeScore', ->
    score = @get 'episodeScore'
    score? and score >= 70

  episodeNegative: Ember.computed 'episodeScore', ->
    score = @get 'episodeScore'
    score? and score < 70

  showMedium: Ember.computed 'showScore', ->
    score = @get 'showScore'
    score? and score >= 50 and score < 85

  showPositive: Ember.computed 'showScore', ->
    score = @get 'showScore'
    score? and score >= 70

  showNegative: Ember.computed 'showScore', ->
    score = @get 'showScore'
    score? and score < 70

  filterComments: Ember.observer 'model', 'filter', ->
    opts =  @get 'model.opts'

    if opts?
      model = @get 'model'
      filter = @get 'filter'

      if filter is 'victims'
        delete opts.user
        opts.stalked_by = @get 'session.username'
      else if filter is 'self'
        delete opts.stalked_by
        opts.user = @get 'session.username'
      else
        delete opts.stalked_by
        delete opts.user

      model.clear()
      model.reload()

  actions:
    nextEpisode: ->
      @transitionToRoute 'comments', @get('userShow'), @get('episode') + 1

    previousEpisode: ->
      @transitionToRoute 'comments', @get('userShow'), @get('episode') - 1

`export default CommentsIndexController`
