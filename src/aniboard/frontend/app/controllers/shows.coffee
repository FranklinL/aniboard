`import Ember from 'ember'`
`import DebouncedPropertiesMixin from 'ember-debounced-properties/mixin'`

FILTER_ALL = 'Show All'
FILTER_NEW = 'Show New Only'

ShowsController = Ember.ArrayController.extend DebouncedPropertiesMixin,
  shows: []
  sortProperties: ['name']
  filterByOptions: [FILTER_ALL, FILTER_NEW]
  filterBy: FILTER_ALL
  searchTerm: ''
  debouncedProperties: ['searchTerm']

  init: ->
    @_super()

    @eventHub.on 'userShow:added', @, @listeners.showAdded

  searchRegex: Ember.computed 'searchTerm', ->
    searchTerm = @get 'searchTerm'
    new RegExp(searchTerm, 'gi') unless searchTerm is ''

  _filterShows: ->
    filterBy = @get 'filterBy'
    searchRegex = @get 'searchRegex'
    shows = @get 'shows'

    if not searchRegex? and filterBy is FILTER_ALL
      shows.setObjects @get 'model'
    else
      shows.setObjects @filter(
        (show) ->
          (not searchRegex? or show.get('name').match(searchRegex)?) and
          (filterBy is FILTER_ALL or show.get('isNew'))
        ,
        @
      )

  filterShows: Ember.observer 'model', 'filterBy', 'debouncedSearchTerm', ->
    shows = @get 'shows'

    if shows.length
      shows.clear()
      Ember.run.later @, @_filterShows, 500
    else
      @_filterShows()

  listeners:
    showAdded: (userShow) ->
      # remove the show from the list and the filtered list
      show = userShow.get 'showSource.show'
      @removeObject show
      @get('shows').removeObject show

`export default ShowsController`
