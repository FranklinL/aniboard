`import Ember from 'ember'`
`import DialogViewMixin from '../../mixins/dialog/view'`

UserShowEditView = Ember.View.extend DialogViewMixin,
  classNames: ['edit']

`export default UserShowEditView`
