`import Ember from 'ember'`

ActionsShowCondensedListView = Ember.View.extend
  tagName: 'section'
  classNames: ['day_list']

`export default ActionsShowCondensedListView`
