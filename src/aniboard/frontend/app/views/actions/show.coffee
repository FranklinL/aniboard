`import Ember from 'ember'`
`import ShowViewMixin from '../../mixins/show/view'`

ActionsShowView = Ember.View.extend ShowViewMixin,
  timeWatchedFormat: 'HH:mm'

`export default ActionsShowView`
