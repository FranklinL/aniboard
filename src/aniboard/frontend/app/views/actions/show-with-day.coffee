`import ActionShowView from './show'`

ActionsShowWithDayView = ActionShowView.extend
  timeWatchedFormat: 'HH:mm dddd'
  templateName: 'actions/show'

`export default ActionsShowWithDayView`
