`import Ember from 'ember'`
`import WeekViewMixin from '../mixins/week/view'`
`import FadeViewMixin from '../mixins/fade-view'`

UserShowsView = Ember.View.extend FadeViewMixin, WeekViewMixin

`export default UserShowsView`
