`import Ember from 'ember'`

HeaderView = Ember.View.extend
  tagName: 'header'
  classNames: 'header'

  actions:
    open: (open) ->
      $nav = @$('nav')

      if open
        $nav.css display: 'block'
        $nav.transition opacity: 1, 'max-height': '500px'
      else
        $nav.transition {opacity: 0, 'max-height': 0}, ->
          $nav.css display: 'none'

`export default HeaderView`
