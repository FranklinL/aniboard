`import Ember from 'ember'`
`import DialogViewMixin from '../mixins/dialog/view'`

FeedbackView = Ember.View.extend DialogViewMixin,
  classNames: ['feedback']

`export default FeedbackView`
