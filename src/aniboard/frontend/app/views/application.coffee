`import Ember from 'ember'`

ApplicationView = Ember.View.extend
  classNames: ['page']

`export default ApplicationView`
