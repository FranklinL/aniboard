`import Ember from 'ember'`
`import ShowDayListViewMixin from '../../mixins/show/day-list-view'`

UserShowsListView = Ember.View.extend ShowDayListViewMixin

`export default UserShowsListView`
