`import Ember from 'ember'`
`import FadeViewMixin from '../mixins/fade-view'`

NoticesView = Ember.View.extend FadeViewMixin,
  tagName: 'section'
  classNames: ['archive']

`export default NoticesView`
