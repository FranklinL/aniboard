`import Ember from 'ember'`
`import DialogViewMixin from 'aniboard/mixins/dialog/view'`

AccountRegisterDoneView = Ember.View.extend DialogViewMixin,
  classNames: ['register']

`export default AccountRegisterDoneView`
