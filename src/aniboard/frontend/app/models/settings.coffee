`import DS from 'ember-data'`

Settings = DS.Model.extend
  askRating: DS.attr 'boolean'
  askComment: DS.attr 'boolean'
  theme: DS.attr 'string'

`export default Settings`
