`import Ember from 'ember'`
`import DS from 'ember-data'`

UserTrace = DS.Model.extend
  trace: DS.attr 'string'

  maxCountUrl: Ember.computed ->
    adapter = @store.adapterFor UserTrace
    baseUrl = adapter.buildURL 'userTrace', @get('id'), @
    "#{baseUrl}max_count/"

  topUser: Ember.computed ->
    Ember.ObjectProxy.extend(Ember.PromiseProxyMixin).create
      promise: new Ember.RSVP.Promise (resolve, reject) =>
        $.ajax(
          url: @get 'maxCountUrl'
          contentType: 'application/json'
          data:
            trace: @get 'trace'
            date_time: moment().startOf('week').format()
        ).done((data) =>
          Ember.run =>
            user = @store.push 'user', @store.normalize('user', data)
            resolve user
        ).fail ->
          Ember.run -> reject()

`export default UserTrace`
