`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

PasswordForgot = DS.Model.extend EmberValidations.Mixin,
  email: DS.attr 'string'

  validations:
    email:
      presence: true
      format:
        with: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
        message: 'must be a valid email'

`export default PasswordForgot`
