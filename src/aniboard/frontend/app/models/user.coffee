`import Ember from 'ember'`
`import DS from 'ember-data'`

User = DS.Model.extend
  username: DS.attr 'string'
  isVictim: DS.attr 'boolean'

  stalkUrl: Ember.computed ->
    adapter = @store.adapterFor User
    baseUrl = adapter.buildURL 'user', @get('id'), @
    "#{baseUrl}stalk/"

  stalk: ->
    new Ember.RSVP.Promise (resolve, reject) =>
      @set 'isStalking', true

      $.ajax(
        type: 'POST'
        url: @get 'stalkUrl'
        contentType: 'application/json'
      ).done((data) =>
        Ember.run =>
          @store.push 'user', @store.normalize('user', data)
          resolve()
      ).fail(->
        Ember.run -> reject()
      ).always =>
        Ember.run =>
          @set 'isStalking', false

`export default User`
