`import Ember from 'ember'`
`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

UserShow = DS.Model.extend EmberValidations.Mixin,
  showSource: DS.belongsTo 'show-source'
  lastEpisodeWatched: DS.attr 'number', defaultValue: 0
  dropped: DS.attr 'boolean', defaultValue: false
  rating: DS.attr 'number'
  score: DS.attr 'number'

  # show source aliases
  show: Ember.computed.readOnly 'showSource.show'
  name: Ember.computed.readOnly 'show.name'
  slug: Ember.computed.readOnly 'show.slug'
  coverImage: Ember.computed.readOnly 'show.coverImage'
  numberEpisodes: Ember.computed.readOnly 'show.numberEpisodes'
  source: Ember.computed.readOnly 'showSource.source.name'
  airingDays: Ember.computed.readOnly 'showSource.airingDays'
  lastAiredEpisode: Ember.computed.readOnly 'showSource.lastAiredEpisode'
  finishedAiring: Ember.computed.readOnly 'showSource.finishedAiring'

  hasScore: Ember.computed.notEmpty 'score'

  completed: Ember.computed 'lastEpisodeWatched', 'numberEpisodes', ->
    @get('lastEpisodeWatched') == @get('numberEpisodes')

  askRating: Ember.computed 'rating', 'completed', ->
    rating = @get 'rating'
    completed = @get 'completed'

    return completed and not rating?

  airingDatetime: (day) ->
    @get('showSource').airingDatetime day

  validations:
    showSource:
      presence: true
    lastEpisodeWatched:
      presence: true
      numericality:
        greaterThanOrEqualTo: 0
      comparator:
        against: 'lastAiredEpisode'
        comparator: (lastEpisodeWatched, lastAiredEpisode)->
          exists = lastEpisodeWatched is null or lastAiredEpisode is null
          exists or parseInt(lastEpisodeWatched) <= parseInt(lastAiredEpisode)
        message: 'this source has only aired %@2 episodes'
    rating:
      numericality:
        allowBlank: true
        onlyInteger: true
        greaterThanOrEqualTo: 0
        lessThanOrEqualTo: 10

`export default UserShow`
