momentCompare = (moment1, moment2) ->
  if moment1? and moment2?
    moment1.valueOf() - moment2.valueOf()
  else if moment1? and not moment2?
    -1
  else if not moment1? and moment2?
    1
  else
    0

`export default momentCompare`
