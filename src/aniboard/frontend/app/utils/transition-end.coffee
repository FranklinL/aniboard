`import Ember from 'ember'`

transitionEnd = ($el, duration, callback) ->
  fired = false
  $el.one $.support.transitionEnd, ->
    fired = true
    callback()

  Ember.run.later (->
    if not fired
      $el.trigger $.support.transitionEnd
  ), duration

`export default transitionEnd`
