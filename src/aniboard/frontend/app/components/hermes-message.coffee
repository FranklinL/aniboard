`import Ember from 'ember'`

`var DEBOUNCE = Ember.testing ? 100 : 4000;`

HermesMessageComponent = Ember.Component.extend
  tagName: 'aside'
  classNames: ['hermes', 'message']
  classNameBindings: ['type']
  message: Ember.computed.alias 'hermes.message'
  type: Ember.computed.alias 'hermes.type'

  showMessage: Ember.observer 'message', ->
    message = @get 'message'

    if message?
      # show the message
      @$().css display: 'initial'
      @$().stop(true).transition opacity: 1

      Ember.run.debounce @, @hideMessage, DEBOUNCE

  hideMessage: ->
    @$().transition(opacity: 0).queue =>
      if !@get('isDestroyed')
        @$().css display: 'none'

`export default HermesMessageComponent`
