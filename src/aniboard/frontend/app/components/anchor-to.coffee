`import Ember from 'ember'`

AnchorToComponent = Ember.Component.extend
  tagName: 'a'
  attributeBindings: ['href']

  href: Ember.computed 'anchorId', ->
    "##{@get 'anchorId'}"

  escapedHref: Ember.computed 'href', ->
    @get('href').replace '.', '\\.'

  click: (event) ->
    event.preventDefault()

    $('html,body').animate
      scrollTop: $(@get('escapedHref')).offset().top

    false

`export default AnchorToComponent`
