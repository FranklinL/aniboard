`import Ember from 'ember'`
`import ShowViewMixin from 'aniboard/mixins/show/view'`

UserShowComponent = Ember.Component.extend ShowViewMixin,
  classNameBindings: ['available::translucent']
  day: null
  model: null
  canEdit: true

  airingDatetime: Ember.computed 'model.airingDatetimes.@each', 'day', ->
    @get('model').airingDatetime @get 'day'

  normalizedEpisodes: Ember.computed 'model.numberEpisodes', 'model.lastAiredEpisode', ->
    @get('model.numberEpisodes') or @get('model.lastAiredEpisode') * 2

  outPercent: Ember.computed 'model.lastAiredEpisode', 'normalizedEpisode', ->
    percent = @get('model.lastAiredEpisode') / @get('normalizedEpisodes') * 100
    "width: #{percent}%".htmlSafe()

  watchedPercent: Ember.computed 'model.lastAiredEpisode', 'normalizedEpisodes', ->
    percent = @get('model.lastEpisodeWatched') / @get('normalizedEpisodes') * 100
    "width: #{percent}%".htmlSafe()

  canWatch: Ember.computed 'model.lastAiredEpisode', 'model.lastEpisodeWatched', ->
    @get('model.lastAiredEpisode') > @get('model.lastEpisodeWatched')

  airsToday: Ember.computed 'airingDatetime', ->
    airingDatetime = @get 'airingDatetime'
    airingDatetime? and airingDatetime.isSame moment(), 'day'

  aired: Ember.computed('airingDatetime', ->
    airingDatetime = @get 'airingDatetime'
    not airingDatetime? or airingDatetime < moment()
  ).volatile()

  available: Ember.computed 'canWatch', 'airsToday', 'aired', ->
    @get('canWatch') or @get('airsToday') and not @get('aired')

  notThisWeek: Ember.computed 'day', ->
    @get('day') is 7

  updateOnAiring: Ember.observer 'clock.time', ->
    time = @get 'clock.time'
    airingDatetime = @get 'airingDatetime'

    if airingDatetime? and time.isSame(airingDatetime, 'minute')
      @get('model').reload()

  actions:
    watchOne: (model) ->
      @sendAction 'watchOne', model

`export default UserShowComponent`
