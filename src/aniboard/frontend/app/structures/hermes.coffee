`import Ember from 'ember'`

`
var MESSAGE_TYPES = {
  INFO: 'info',
  SUCCESS: 'success',
  ERROR: 'error',
}
`

Hermes = Ember.Object.extend
  timeout: 4000
  message: null
  type: MESSAGE_TYPES.INFO

  show: (message, type) ->
    @set 'message', message
    @set 'type', type

  info: Ember.computed 'message', (key, message) ->
    if arguments.length > 1
      @show message, MESSAGE_TYPES.INFO

    @get 'message'

  success: Ember.computed 'message', (key, message) ->
    if arguments.length > 1
      @show message, MESSAGE_TYPES.SUCCESS

    @get 'message'

  error: Ember.computed 'message', (key, message) ->
    if arguments.length > 1
      @show message, MESSAGE_TYPES.ERROR

    @get 'message'

`export {MESSAGE_TYPES, Hermes}`
`export default Hermes`
