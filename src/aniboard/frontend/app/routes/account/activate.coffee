`import Ember from 'ember'`
`import UnauthenticatedRouteMixin from 'simple-auth/mixins/unauthenticated-route-mixin'`

AccountActivateRoute = Ember.Route.extend UnauthenticatedRouteMixin,
  titleToken: 'Activate'

  model: (params) ->
    model = @store.createRecord 'userActivation',
      activationCode: params.activation_code

    @set 'hermes.info','Activating...'
    model.save().then( =>
      @set 'hermes.success', 'Your account has been successfully activated.
        You can login now.'

      # send ga event
      @get('ga').send 'event', 'account', 'activate'
    ).catch( =>
      @set 'hermes.error', 'The activation code is invalid.'
    ).finally =>
      @transitionTo 'account.login'

`export default AccountActivateRoute`
