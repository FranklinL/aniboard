`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

ShowAddRoute = Ember.Route.extend DialogRouteMixin, AuthenticatedRouteMixin,
  titleToken: 'Add'

  model: ->
    show = @modelFor 'show'
    promises = []

    # fetch the user show if it exists
    promises.pushObject @store.find('userShow',
      user: @get 'session.username'
      show: show.get 'id'
    ).then (result) =>
      if result and result.get('length') is 1
        result.get 'firstObject'
      else
        @store.createRecord 'userShow'

    # fetch the sources for the show
    promises.pushObject show.get('showSources').then (result) ->
      result.toArray()

    Ember.RSVP.all promises

  afterModel: (model, transition) ->
    userShow = model[0]

    if not userShow.get('isDirty') and userShow.get('dropped') is false
      # we don't want to add it again, go to edit
      @transitionTo 'userShow.edit', userShow

  setupController: (controller, model) ->
    controller.set 'model', model[0]
    controller.set 'sources', model[1]
    controller.set 'show', @modelFor 'show'

  renderTemplate: ->
    @render
      outlet: 'dialog'
      into: 'application'

`export default ShowAddRoute`
