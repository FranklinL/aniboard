`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

UserShowsRoute = Ember.Route.extend AuthenticatedRouteMixin,
  titleToken: 'Watching'

  model: ->
    @store.find('userShow',
      user: @get 'session.username'
      dropped: 'False'
    ).then (data) ->
      data.toArray()

  clockUpdate: Ember.observer 'clock.dayChanged', ->
    dayChanged = @get 'clock.dayChanged'

    if dayChanged
      @refresh()

`export default UserShowsRoute`
