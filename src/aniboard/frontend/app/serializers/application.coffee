`import Ember from 'ember'`
`import DS from 'ember-data'`

ApplicationSerializer = DS.RESTSerializer.extend DS.EmbeddedRecordsMixin,
  attrs:
    showSource:
      serialize: 'ids'
      deserialize: 'records'
    show:
      serialize: false
      deserialize: 'records'
    source:
      serialize: false
      deserialize: 'records'
    airingDatetimes:
      serialize: false
      deserialize: 'records'
    userShow:
      serialize: false
      deserialize: 'records'
    comment:
      serialize: false
      deserialize: 'records'
    user:
      serialize: false
      deserialize: 'records'

  keyForAttribute: (attr) ->
    Ember.String.decamelize attr

  keyForRelationship: (attr) ->
    Ember.String.decamelize attr

  extractMeta: (store, type, payload) ->
    if payload.results?
      store.setMetadataFor type,
        pagination:
          pages: payload.pages
          page: payload.page

  wrapPayload: (type, payload) ->
    newPayload = {}
    newPayload[type.typeKey] = if payload.results? then payload.results else payload
    newPayload

  extractSingle: (store, type, payload) ->
    @_super store, type, @wrapPayload(type, payload)

  extractArray: (store, type, payload) ->
    @_super store, type, @wrapPayload(type, payload)

`export default ApplicationSerializer`
