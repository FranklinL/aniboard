`import ApplicationSerializer from './application'`

UserShowCommentSerializer = ApplicationSerializer.extend
  attrs:
    userShow:
      serialize: 'ids'
      deserialize: 'ids'
    user:
      serialize: false
      deserialize: 'records'

`export default UserShowCommentSerializer`
