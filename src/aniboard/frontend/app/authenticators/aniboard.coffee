`import Ember from 'ember'`
`import BaseAuthenticator from 'simple-auth/authenticators/base'`

AniboardAuthenticator = BaseAuthenticator.extend
  apiSessionURL: '/api/session/'

  restore: ->
    new Ember.RSVP.Promise (resolve, reject) =>
      $.ajax(
        url: @get 'apiSessionURL'
      ).done((data) ->
        if data.authenticated
          resolve data
        else
          reject new Error 'Failed to restore session'
      ).fail -> reject new Error 'Failed to restore session'

  authenticate: (options) ->
    new Ember.RSVP.Promise (resolve, reject) =>
      $.ajax(
        type: 'post'
        url: @get 'apiSessionURL'
        data:
          JSON.stringify
            username: options.identification
            password: options.password
        contentType: 'application/json'
      ).done((data) ->
        if data.authenticated
          resolve data
        else
          reject new Error 'Failed to authenticate'
      ).fail -> reject new Error 'Failed to authenticate'

  invalidate: (data) ->
    new Ember.RSVP.Promise (resolve, reject) =>
      $.ajax(
        type: 'delete'
        url: @get 'apiSessionURL'
      ).done((data) ->
        if not data.authenticated
          resolve()
        else
          reject new Error 'Failed to invalidate session'
      ).fail -> reject new Error 'Failed to invalidate session'

`export default AniboardAuthenticator`
