# -*- coding: utf-8 -*-
import os
import celery.schedules as celery_schedules


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '26e*ops0%*t)gl@odsdnos@@(c@@sw%*r+$zkr6us7i5058w6_'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = DEBUG

# Application definition

INSTALLED_APPS = (
    'modeltranslation',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'django_orphaned',
    'dbbackup',
    'aniboard.core',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'aniboard.urls'

WSGI_APPLICATION = 'aniboard.wsgi.application'

ALLOWED_HOSTS = '*'

# Admins and managers
ADMINS = ()
MANAGERS = ()

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'sqlite.db',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
LANGUAGE_CODE = 'en'

TIME_ZONE = 'UTC'

USE_TZ = True

USE_I18N = True

USE_L10N = True

LANGUAGES = (
    ('en', 'English'),
    ('ja', '日本語'),
)


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'aniboard', 'frontend', 'dist'),
)

# Media files
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# site
SITE_ID = 1

# Orphaned
ORPHANED_APPS_MEDIABASE_DIRS = {
    'core': {
        'root': MEDIA_ROOT,
    }
}

# Login config
LOGIN_URL = '/login'
LOGIN_REDIRECT_URL = '/'

# Email
EMAIL_SUBJECT_PREFIX = 'Tsuiseki - '

# Backups
DBBACKUP_BACKUP_DIRECTORY = os.path.join(BASE_DIR, 'backups')
DBBACKUP_CLEANUP_KEEP = 30

# Rest framework config
REST_FRAMEWORK = {
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_MODEL_SERIALIZER_CLASS':
    'rest_framework.serializers.HyperlinkedModelSerializer',
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),
}

# Celery
BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_SEND_TASK_ERROR_EMAILS = True
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_EVENT_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_ROUTES = {
    'aniboard.core.tasks.create_per_backend_tasks': {'queue': 'sync'},
    'aniboard.core.tasks.call_backend_api': {'queue': 'sync'},
    'aniboard.core.tasks.update_last_aired_episodes': {'queue': 'shows'},
    'aniboard.core.tasks.update_airing_datetimes': {'queue': 'shows'},
    'aniboard.core.tasks.update_finished_airing_shows': {'queue': 'shows'},
    'aniboard.core.tasks.delete_finished_airing_shows': {'queue': 'shows'},
}
CELERYBEAT_SCHEDULE = {
    'update_last_aired_episodes': {
        'task': 'aniboard.core.tasks.update_last_aired_episodes',
        'schedule': celery_schedules.crontab(minute=0),
    },
    'update_missing_episode_count': {
        'task': 'aniboard.core.tasks.update_missing_episode_count',
        'schedule': celery_schedules.crontab(
            hour=0, minute=0, day_of_week='0,2,5'
        )
    },
    'scrape_season_start_month': {
        'task': 'aniboard.core.tasks.scrape_season',
        'schedule': celery_schedules.crontab(
            hour='00,12',
            minute=00,
            month_of_year='1,4,7,10',
            day_of_month='1-7'
        ),
    },
    'scrape_season_end_month': {
        'task': 'aniboard.core.tasks.scrape_season',
        'schedule': celery_schedules.crontab(
            hour='00,12',
            minute=00,
            month_of_year='3,6,9,12',
            day_of_month='17-31'
        ),
    },
    'find_breaks': {
        'task': 'aniboard.core.tasks.find_breaks',
        'schedule': celery_schedules.crontab(hour=0, minute=0),
    },
    'update_syoboi': {
        'task': 'aniboard.core.tasks.update_syoboi',
        'schedule': celery_schedules.crontab(hour=0, minute=0),
    },
    'update_airing_datetimes': {
        'task': 'aniboard.core.tasks.update_airing_datetimes',
        'schedule': celery_schedules.crontab(
            hour=0, minute=0, day_of_week='0'
        ),
    },
    'update_finished_airing_shows': {
        'task': 'aniboard.core.tasks.update_finished_airing_shows',
        'schedule': celery_schedules.crontab(
            hour=0, minute=0, day_of_week='0'
        ),
    },
    'delete_finished_airing_shows': {
        'task': 'aniboard.core.tasks.delete_finished_airing_shows',
        'schedule': celery_schedules.crontab(
            hour=0, minute=0, day_of_week='0'
        ),
    },
    'delete_user_traces': {
        'task': 'aniboard.core.tasks.delete_user_traces',
        'schedule': celery_schedules.crontab(
            hour=0, minute=0, day_of_week='0'
        ),
    },
    'clean_old_images': {
        'task': 'aniboard.core.tasks.clean_old_images',
        'schedule': celery_schedules.crontab(
            hour=12, minute=0, day_of_week='0'
        ),
    },
    'backup_database': {
        'task': 'aniboard.core.tasks.backup_database',
        'schedule': celery_schedules.crontab(
            hour=12, minute=0, day_of_week='0'
        ),
    },
}

from aniboard.private_settings import *  # noqa
