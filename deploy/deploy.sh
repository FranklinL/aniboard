#!/bin/bash
# deploy dir
BASE_DIR=`dirname ${BASH_SOURCE[0]}`

# base command
command="ansible-playbook "

# playbook 
playbook="$BASE_DIR/deploy.yml"

# base skip tags
skip=("static" "requirements" "django" "configuration")
declare -A variables
extra=()

# retrieve other options
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        -pb|--branch)
            shift

            if [ -z "${1##-*}" ]
            then
                echo "You must indicate the branch, not another option"
                exit 1
            fi

            variables[branch]=$1
        ;;
        -ps|--static)
            unset skip[0]
        ;;
        -pr|--requirements)
            unset skip[1]
        ;;
        -pd|--django)
            unset skip[2]
        ;;
        -pc|--configuration)
            unset skip[3]
        ;;
        --initial)
            playbook="$BASE_DIR/initial_deploy.yml"
            unset skip
        ;;
        *)
            extra+=($1)
        ;;
    esac
    shift
done

# build the rest of the command an execute it
command+="${playbook}"

if [ ${#variables[@]} -ne 0 ]
then
    vars=""

    for i in "${!variables[@]}"
    do
        vars+="${i}=${variables[$i]}"   
    done

    command+=" -e \"${vars}\""
fi

if [ ${#skip[@]} -ne 0 ]
then
    skip=$( IFS=,; echo "${skip[*]}" )
    command+=" --skip-tags=\"${skip}\""
fi

if [ ${#extra[@]} -ne 0 ]
then
    extra=$( IFS=' '; echo "${extra[*]}" )
    command+=" ${extra}"
fi

echo "Running <${command}>..."
eval $command
